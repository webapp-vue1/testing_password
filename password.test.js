/* eslint-disable indent */
/* eslint-disable eol-last */

const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Lenght', () => {
    test('should 8 characters to be true', () => {
        expect(checkLength('12345678')).toBe(true)
    })
    test('should 7 characters to be false ', () => {
        expect(checkLength('1234567')).toBe(false)
    })
    test('should 25 characters to be false ', () => {
        expect(checkLength('1212121212121212121212121')).toBe(true)
    })
    test('should 26 characters to be false ', () => {
        expect(checkLength('12121212121212121212121212')).toBe(false)
    })
})

describe('Test Password Alphabet', () => {
    test('should has alphabet m in password to be true', () => {
        expect(checkAlphabet('m')).toBe(true)
    })

    test('should has not alphabet A in password to be true', () => {
        expect(checkAlphabet('A')).toBe(true)
    })

    test('should has not alphabet Z in password to be true', () => {
        expect(checkAlphabet('Z')).toBe(true)
    })

    test('should has not alphabet in password to be false', () => {
        expect(checkAlphabet('1111')).toBe(false)
    })
})

describe('Test Password Digit', () => {
    test('should has digit 0 in password to be true', () => {
        expect(checkDigit('0')).toBe(true)
    })

    test('should has digit 9 in password to be true', () => {
        expect(checkDigit('9')).toBe(true)
    })

    test('should has not digit in password to be false', () => {
        expect(checkDigit('abc')).toBe(false)
    })
})

describe('Test Password Symbol', () => {
    test('should has symbol ! in password to be true', () => {
        expect(checkSymbol('11!11')).toBe(true)
    })

    test('should has symbol }~ in password to be true', () => {
        expect(checkSymbol('11}~11')).toBe(true)
    })

    test('should has not symbol in password to be false', () => {
        expect(checkSymbol('0123asd')).toBe(false)
    })
})

describe('Test Password', () => {
    test('should password Cde@123 to be false', () => {
        expect(checkPassword('Cde@123')).toBe(false)
    })

    test('should password Abc#1234 to be true', () => {
        expect(checkPassword('Abc#1234')).toBe(true)
    })
})